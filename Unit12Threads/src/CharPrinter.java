

public class CharPrinter implements Runnable {

	private char Char;
	private long milliseconds;

	public CharPrinter(char Char, long milliseconds) {
		this.Char = Char;
		this.milliseconds = milliseconds;
	}
	
	public char getChar() {
		return Char;
	}

	@Override
	public void run() {
		int counter =0;
		try {
		while(true) {
			System.out.print(Char);
			counter++;
			if(counter >= 5) {
			System.out.println();
			counter=0;
			}
			Thread.sleep(milliseconds);
		}
		}catch (InterruptedException e ) {
			System.err.print(Char + "Interrupted");
		}
	}

}
