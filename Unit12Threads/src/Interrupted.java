import java.util.*;

public class Interrupted implements Runnable{
	private Thread t;
	public Interrupted(Thread t) {
		this.t=t;
	}
	@Override
	public void run() {
		try {
		long x = (long) (Math.random() * 20000 + 10000);
		Thread.sleep(x);
		t.interrupt();
		}catch (InterruptedException e) {

		}
	}
	
	

}
