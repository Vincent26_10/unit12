import java.util.*;
public class CharPrinterMain {

	public static void main(String[] args) {
		/*Runnable a = new CharPrinter('a', 1000);
		Thread t = new Thread(a);
		t.start();*/
		
		List<Runnable> runnableList = new ArrayList<Runnable>();
		
		for(char c ='a' ; c <= 'z'; c++) {
			long x = (long) (Math.random() * 3000 + 3000);
			Runnable r = new CharPrinter(c,x);
			runnableList.add(r);
		}
		launchThread(runnableList);
	}
	
	public static void launchThread(List<Runnable> list) {
		for(Runnable r:list) {
			Thread t = new Thread(r);
			Interrupted interr = new Interrupted(t);
			Thread interrupted = new Thread(interr);
			t.start();
			interrupted.start();

		}
	}

}
