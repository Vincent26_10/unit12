class MyRunnable implements Runnable{
	@Override
	public void run(){
		try {
		for(int i=0; i<10000; i++) {
			System.out.println("MyThread: " + i);
			Thread.sleep(1000);
		}
		
		}catch (InterruptedException e) {
			
		}
	}
}

public class ThreadInterrumpted {

	public static void main(String[] args) {
		
		Thread t  = new Thread(new MyRunnable());
		t.start();
		
		for(int i = 0; i<5;i++ ) {
			System.out.println("Main " +i);
		}
		t.interrupt();
		for(int i = 5; i<20;i++ ) {
			System.out.println("Main " +i);
		}
	}

}
