class MyThread extends Thread {
	public void run() {
		for(int i=0;i<10000;i++) {
			System.out.println("Mythread " +i);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}


public class Threads1 {

	public static void main(String[] args)  throws InterruptedException{
		MyThread t = new MyThread();
		t.start();
		for(int i=0;i<10000;i++) {
			System.out.println("main " +i);
			Thread.sleep(100);
		}

	}

}
